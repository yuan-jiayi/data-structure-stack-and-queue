#include<iostream>
#include<string>
using namespace std;
typedef struct BTNode
{
	string name;
	bool isfile;//判断该结点是否为文件 true--文件 false--目录
	/*需要判断优先级，目录优先级大于文件优先级*/
	struct BTNode* brother;//左子树存兄弟
	struct BTNode* child;//右子树存孩子
}BTNode, * BTree;
void InitList(BTree& temp, BTree& bt);//目录插入
void InitFile(BTree& temp, BTree& bt);//文件插入
void CreateTree(BTree& bt, string str, int i);//建树
void PreDispTree(BTree bt, int h);//先序输出

int main()
{
	int N;
	string str;
	int i = 0;
	int h = 0;
	BTree T;//初始化树，建立一棵只有根节点root的二叉树
	T = new BTNode;
	T->name = "root";
	T->brother = T->child = NULL;

	cin >> N;
	for (i = 0; i < N; i++)
	{
		cin >> str;
		CreateTree(T, str, 0);//建树
	}
	PreDispTree(T, h);//先序输出
	return 0;
}

void CreateTree(BTree& bt, string str, int i)//建树
{
	if (str.size() <= i)
		return;
	BTree temp, btr;
	btr = bt;
	temp = new BTNode;
	temp->brother = temp->child = NULL;
	temp->isfile = true;

	/*若待插入优先级比链上这个结点大 或者 优先级相同但待插入结点字典序较小 就直接插入链上这个结点Node的前面即可*/
	/*若优先级比链上这个结点小 或者 优先级相同但是字典序大于链上这个结点Node的话我们就继续沿着这条链往后寻找*/
	/*最极端的情况是找到了链的末尾，那么链的末尾就是我们带插入元素的位置*/
	while (i < str.size() && str[i] != '\\')
	{
		temp->name += str[i];
		i++;
	}
	if (str[i] == '\\')//temp为目录
	{
		temp->isfile = false;
		i++;
	}
	if (temp->isfile == true)//temp为文件
		InitFile(temp, btr);//文件插入
	else//temp为目录
	{
		InitList(temp, btr);//目录插入
		CreateTree(temp, str, i);
	}
}

/*如果该文件或目录还没有创建，那么创建的一定是其儿子节点
如果已经创建完毕了，那么插入的位置一定在该节点的左子树中查找*/

void InitList(BTree& temp, BTree& bt)//目录插入
{
	BTree btr;//遍历二叉树bt
	btr = bt->child;
	//先对第一个兄弟结点进行判断
	if (btr == NULL || btr->isfile == true || temp->name < btr->name)//temp在第一个位置，可插入：btr为文件
	{
		temp->brother = btr;
		bt->child = temp;//修改孩子指针
	}
	else if (btr->name == temp->name)//重复目录
		temp = btr;
	//对第二个兄弟结点进行判断
	else
	{
		while (btr->brother != NULL)//btr为前驱指针
		{
			if (btr->brother->isfile == true || temp->name < btr->brother->name)//若下一兄弟结点为文件或者目录
				break;
			else if (btr->brother->name == temp->name)//目录名字重复
			{
				temp = btr->brother;
				break;
			}
			else
				btr = btr->brother;
		}
		if (btr->brother == NULL || btr->brother->name != temp->name)
		{
			temp->brother = btr->brother;
			btr->brother = temp;
		}
	}
}

void InitFile(BTree& temp, BTree& bt)//文件插入
{
	BTree btr;
	btr = bt->child;
	//先对第一个兄弟结点进行判断
	if (btr == NULL || (btr->isfile == true && btr->name >= temp->name))
	{
		temp->brother = btr;
		bt->child = temp;
	}
	else//对第二个兄弟结点进行判断
	{
		while (btr->brother != NULL)
		{
			if (btr->brother->isfile == true && bt->brother->name >= temp->name)
				break;
			else
				btr = btr->brother;
		}
		temp->brother = btr->brother;
		btr->brother = temp;
	}
}

void PreDispTree(BTree bt, int h)//先序输出
{
	if (bt == NULL)
		return;
	for (int i = 0; i < h; i++)
		cout << "  ";
	cout << bt->name;
	cout<< endl;
	PreDispTree(bt->child, h + 1);
	PreDispTree(bt->brother, h);
}

