#include <iostream>
#include <string>
#include <queue>
using namespace std;
typedef char ElemType;
typedef struct BiTNode
{
	ElemType data;
	struct BiTNode* lchild, * rchild;
}*BTree, BiTree;
BTree CreatTree(string str, int& i);//建树
void LevelOrder(BTree bt);//层次遍历
int main()
{
	int i = 0;
	string str;
	cin >> str;
	BTree bt = CreatTree(str, i);
	LevelOrder(bt);
	return 0;
}
BTree CreatTree(string str, int& i)//建树
{
	BTree bt;
	if (str[i] == '#')
		return NULL;
	if (i > str.size() - 1)
		return NULL;
	bt = new BiTree;
	bt->data = str[i];
	bt->lchild = CreatTree(str, ++i);
	bt->rchild = CreatTree(str, ++i);
	return bt;
}

void LevelOrder(BTree bt)//层次遍历
{
	int level = 0;//层数
	int flag = 0;//判断是否为第一层结点
	BTree node, lastNode; //分别用于存放遍历中途结点的孩子结点并判断是否找到这一层的最后一个结点
	node = lastNode = bt;
	BTree p;
	p = new BiTree;
	p->lchild = NULL;
	p->rchild = NULL;
	queue<BTree>qtree;//存放结点

	if (bt != NULL)//二叉树不为空
		qtree.push(bt);
	else//二叉树为空
	{
		cout << "NULL";
		return;
	}
	while (!qtree.empty())//队列不空
	{
		if (node == lastNode)//若找到这一层的最后一个结点
		{
			level++;//层层递增
			if (flag == 0)
			{
				cout << level << ":";
				flag = 1;//将flag置为1,即只在第一次循环时输出1
			}
			else
			{
				cout << endl;
				cout << level << ":";
			}
			lastNode = qtree.back();//取队尾元素
		}

		node = qtree.front();
		cout << node->data << ",";
		//左右孩子入队
		if (node->lchild)
			qtree.push(node->lchild);
		if (node->rchild)
			qtree.push(node->rchild);
		qtree.pop();
	}
}
