#include <iostream>
#include <queue>
#include <string>
using namespace std;
int main()
{
	queue<int>A, B;
	int N, num;//顾客总数  顾客编号
	int i, j;
	int front;//保持队列的头
	int flag = 0;//格式

	cin >> N;
	for (i = 0; i < N; i++)//遍历入队顾客编号
	{
		cin >> num;
		if (num % 2 != 0)//奇数--A
			A.push(num);
		else     //偶数--B
			B.push(num);
	}

	for (i = 0; i < N; i++)
	{
		j = 0;//A窗口处理顾客人数下标
		while (j < 2)//A窗口未解决完2个客户
		{
			j++;
			if (!A.empty())//A不空
			{
				front = A.front();
				A.pop();
				if (flag == 0)
				{
					cout << front;
					flag = 1;
				}
				else
					cout << " " << front;
			}
		}

		if (!B.empty())//A窗口解决2人
		{
			front = B.front();
			B.pop();
			if (flag == 0)
			{
				cout << front;
				flag = 1;
			}
			else
				cout << " " << front;
		}
	}
	return 0;
}