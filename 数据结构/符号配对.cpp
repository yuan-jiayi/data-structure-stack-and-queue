#include <iostream>
#include <stack>
#include <string.h>
using namespace std;
int MatchBracket(stack<char>& p, char bot);//判断右边符号是否与栈顶元素匹配
int CheckBracket(stack<char>& p, char str[100]);

int main() 
{
    stack<char>p;
    char str[100];
    char bot;//取栈底元素
    int flag;
    while (cin >> str)
    {
        if (str[0] == '.' || !str)//结束标志
            break;
        flag = CheckBracket(p, str);
        if (flag == -1)
            return 0;
    }

	//开头是否有多余的符号判断
    if (p.empty())
        cout << "YES" << endl;//全部匹配
    else
    {
        cout << "NO" << endl;
        bot = p.top();
        if (bot == '*')
            cout << "/*" << "-?" << endl;
        else
            cout << bot << "-?" << endl;//缺少右括号
    }
    return 0;
}

int CheckBracket(stack<char>& p, char str[100])
{
    int i = 0;
    int len;
    len = strlen(str);
    int flag = 0;
    for (i = 0; i < len; i++)
    {
        if (str[i] == '(' || str[i] == '[' || str[i] == '{')
            p.push(str[i]);//入栈
		else if (str[i] == '/' && str[i + 1] == '*')
		{
			p.push('*');
			i++;//同时判断两个符号，下标要多移一位
		}
        else if (str[i] == '*' && str[i + 1] == '/')
        {
            flag = MatchBracket(p, '*');
            if (flag == -1)//不匹配
                return -1;
            i++;//同时判断两个符号，下标要多移一位
        }
        else if (str[i] == '*' && str[i - 1] != '/' && str[i + 1] != '/')
            continue;
        else
        {
            flag = MatchBracket(p, str[i]);
            if (flag == -1)//不匹配
                return -1;
        }
    }
    return 0;
}

int MatchBracket(stack<char>& p, char bot)//判断右边符号是否与栈顶元素匹配
{
	if (bot == ')' || bot == ']' || bot == '}' || bot == '*')
	{
		if (p.empty())//栈空
		{
			cout << "NO" << endl;//缺少左括号
			if (bot == '*')
				cout << "?-" << "*/" << endl;
			else
				cout << "?-" << bot << endl;
			return -1;
		}
		else
		{
			switch (bot)
			{
			case')':
				if (p.top() == '(')//匹配成功
					p.pop();
				else
				{
					cout << "NO" << endl;
					if (p.top() == '*')//缺少左括号
						cout << "/*" << "-?" << endl;
					else
						cout << p.top() << "-?" << endl;
					return -1;
				}
				break;

			case']':
				if (p.top() == '[')//匹配成功
					p.pop();
				else
				{
					cout << "NO" << endl;
					if (p.top() == '*')//缺少左括号
						cout << "/*" << "-?" << endl;
					else
						cout << p.top() << "-?" << endl;
					return -1;
				}
				break;

			case'}':
				if (p.top() == '{')//匹配成功
					p.pop();
				else
				{
					cout << "NO" << endl;
					if (p.top() == '*')//缺少左括号
						cout << "/*" << "-?" << endl;
					else
						cout << p.top() << "-?" << endl;
					return -1;
				}
				break;

			case'*':
				if (p.top() == '*')//匹配成功
					p.pop();
				else
				{
					cout << "NO" << endl;
					if (p.top() == '*')//缺少左括号
						cout << "/*" << "-?" << endl;
					else
						cout << p.top() << "-?" << endl;
					return -1;
				}
				break;

			}
		}
	}
	return 0;
}